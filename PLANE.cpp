#include "myheader.h"
#include "model.h"
#include "bullet.h"
#include "block.h"
#include "grade.h"
#include "cstdlib"
#include <cstdlib>

#define bullet_color RED+LIGHT

//using namespace PLANE;
int main()
{
	system("cls");
	bool blod[10];//決定'*blo[]'是否為空
	int order=0;//決定要發射第幾號'b[]'
	plane p1(RED);//飛機
	//最多有十個飛彈
	vector<bullet> b;//={bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT),bullet(RED+LIGHT)};
	for(int i=0;i<10;++i)
		b.push_back(bullet(bullet_color));
	srand(time(0));//隨機亂數
	block *blo[10];//最多十個方塊
	int tclock=0;//代表上次的時間
	int up=1;//目前能出現幾個方塊
	//初始化每個'*blo[]'都有值
	for(int i=0;i<10;i++)
		blod[i]=1;
	//初始化給每個'blo[]'數值;
	for(int i=0;i<10;i++)
		blo[i]=new block(rand());
	//畫飛機可移動範圍
	gotoxy(0,22);
	for(int i=0;i<25;i++)
	{
		color(BLUE,5);
	}
	for(int i=0;i<3;i++)
	{
		gotoxy(25,22+i);
		color(BLUE,5);
	}
	//print level
	gotoxy(30,8);
	cout <<"level:" <<0;
	gotoxy(36,5);
	cout <<"/100";
	//開始玩
	
	while(1)
	{
		
		order%=10;//飛彈號碼不能超過十
		if(_kbhit())//如果鍵盤有輸入
		{
			if(p1.getlevel()==9)
			{
				gotoxy(35,12);
				cout <<"You can press 'q' to exit";
			}
			int d=direction();//鍵盤輸入值給'd'
			
			if(d==32)//32 空白鍵
			{
				p1.shoot(&b[order]);//發射飛彈
				order++;//下次發射的飛彈編號+1
			}
			if(d=='q')//113 q
				break;//離開不玩了
			else
				p1.move(d);//移動飛機
			
		}
		for(int i=0;i<10;i++)
			b[i].print();//印出飛彈軌跡
		PLANE::point.print();//印出分數
		
		//當現在時間和上次時間差1300以上，新增一個block，但是最多十個
		if(up<10 && clock()-tclock>=PLANE::twoblockgap[0])
		{
			up++;//準備新增下一號block
			tclock=clock();//更新時間
		}
		//只執行目前已新增的block
		for(int i=0;i<up;i++)
		{
			//如果'*blo[i]'有值，則執行
			//'*blo[i]'不能為空
			if(blod[i])
			{
				if(blo[i]->print()==0)//blo[i]被幹掉
				{
					delete blo[i];
					blod[i]=0;//'*blo[i]'為空
					p1.setexperience(PLANE::point.getpoints());//add experience
					
				}
			}
			//當現在時間和上次時間差700以上，新增一個原為空的blo[i]
			if(clock()-tclock>=PLANE::twoblockgap[blo[0]->getvlev()] && blod[i]==0)
			{
				blo[i]=new block(rand());
				tclock=clock();//更新時間
				blod[i]=1;//'*blo[i]'有值
			}
			
		}
		
		
	}
	//'*blo[]'有值則刪
	for(int i=0;i<10;i++)
		if(blod[i]==1)
			delete blo[i];
	system("PAUSE");
	return 0;
}