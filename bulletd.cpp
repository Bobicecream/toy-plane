#include "bullet.h"
#include "myheader.h"

bullet::bullet(const int c)
{
	color=c;//the color of the bullet is c
	s=0;//not exist
	i=1;
	t=clock();
	bulletlevel=1;
}
void bullet::print()
{
	//exists then print
	if(s)
	{
		
		move(bulletlevel);
		//if exceed bound then initialize data
		if(i>=23)
		{
			s=0;
			i=1;
		}
	}
}
void bullet::move(const int level)
{
	if(clock()-t>=60 && i<=22)
	{
		t=clock();
		if(site[x][22-i]==4)
		{
			if(i!=22)
				site[x][22-i]=2;
			site[x][23-i]=0;
			
			if(i!=1)
			{
				gotoxy(x,23-i);
				printandclear(0,x,23-i,color,"|");
			}
			i=23;
		}
		else
		{
			switch(level)
			{
			case 1:
				//particularly i=1
				if(i==1)
				{
					printandclear(1,x,21,color,"|");
					site[x][21]=2;
				}
				//particularly i=23
				else if(i==22)
				{
					printandclear(0,x,1,color,"|");
					site[x][1]=0;
				}
				//move front delete back
				//site[][]=2, there is a bullet
				else
				{
					printandclear(1,x,22-i,color,"|");
					printandclear(0,x,23-i,color,"|");
					site[x][22-i]=2;
					site[x][23-i]=0;
				}
				i++;//move front
				break;
			case 2:
				//particularly i=1
				if(i==1)
				{
					printandclear(1,x-1,21,color,"|||");
					for(int j=-1;j<=1;j++)
						site[x+j][21]=2;
				}
				//particularly i=23
				else if(i==22)
				{
					printandclear(0,x-1,1,color,"|||",3);
					for(int j=-1;j<=1;j++)
						site[x+j][1]=0;
				}
				//move front delete back
				//site[][]=2, there is a bullet
				else
				{
					printandclear(1,x-1,22-i,color,"|||");
					printandclear(0,x-1,23-i,color,"|||",3);
					for(int j=-1;j<=1;j++)
					{
						site[x+j][22-i]=2;
						site[x+j][23-i]=0;
					}
				}
				i++;//move front
				/*bullet bl(color),br(color);
				bl.x=x-1;
				br.x=x+1;
				bl.s=1;
				br.s=1;
				br.i=i;
				bl.i=i;
				bl.t=t-60;
				br.t=t-60;
				bl.move(1);
				br.move(1);
				t=t-60;
				move(1);*/
				break;
			case 3:
				//particularly i=1
				if(i==1)
				{
					
					for(int j=-2;j<=2;j++)
					{
						if(x+j<0)
							continue;
						if(x+j==25)
							break;
						printandclear(1,x+j,21,color,"|");
						site[x+j][21]=2;
					}
				}
				//particularly i=23
				else if(i==22)
				{
					
					for(int j=-2;j<=2;j++)
					{
						if(x+j<0)
							continue;
						if(x+j==25)
							break;
						printandclear(0,x+j,1,color,"|");
						site[x+j][1]=0;
					}
				}
				//move front delete back
				//site[][]=2, there is a bullet
				else
				{
					
					
					for(int j=-2;j<=2;j++)
					{
						if(x+j<0)
							continue;
						if(x+j==25)
							break;
						printandclear(1,x+j,22-i,color,"|");
						printandclear(0,x+j,23-i,color,"|");
						site[x+j][22-i]=2;
						site[x+j][23-i]=0;
					}
				}
				i++;//move front
				break;
			case 4:
				//particularly i=1
				if(i==1)
				{
					
					for(int j=-7;j<=7;j++)
					{
						if(x+j<0)
							continue;
						if(x+j==25)
							break;
						printandclear(1,x+j,21,color,"|");
						site[x+j][21]=2;
					}
				}
				//particularly i=23
				else if(i==22)
				{
					
					for(int j=-7;j<=7;j++)
					{
						if(x+j<0)
							continue;
						if(x+j==25)
							break;
						printandclear(0,x+j,1,color,"|");
						site[x+j][1]=0;
					}
				}
				//move front delete back
				//site[][]=2, there is a bullet
				else
				{
					
					
					for(int j=-7;j<=7;j++)
					{
						if(x+j<0)
							continue;
						if(x+j==25)
							break;
						printandclear(1,x+j,22-i,color,"|");
						printandclear(0,x+j,23-i,color,"|");
						site[x+j][22-i]=2;
						site[x+j][23-i]=0;
					}
				}
				i++;//move front
				break;
			}	
		}
	}
	/*switch(level)
	{
	case 1:
		if(clock()-t>=60 && i<=22)//do not exceed bound
		{
			t=clock();//renew time
			if(site[x][22-i]==4)
			{
				if(i!=1)
					printandclear(0,x,23-i,color,"|");
				//site[x][22-i]=5;
				site[x][23-i]=0;
				i++;
			}
			else if(site[x][23-i]==4)
			{
				if(i!=22)
				{
					printandclear(1,x,22-i,color,"|");
					site[x][22-i]=2;
					
				}
					//site[x][23-i]=0;
				i++;
			}
			else
			{
				//particularly i=1
				if(i==1)
				{
					printandclear(1,x,21,color,"|");
					site[x][21]=2;
				}
				//particularly i=23
				else if(i==22)
				{
					printandclear(0,x,1,color,"|");
					site[x][1]=0;
				}
				//move front delete back
				//site[][]=2, there is a bullet
				else
				{
					printandclear(1,x,22-i,color,"|");
					printandclear(0,x,23-i,color,"|");
					site[x][22-i]=2;
					site[x][23-i]=0;
				}
				i++;//move front
			}
			
		}
		break;
	case 2:
		if(clock()-t>=60 && i<=22)//do not exceed bound
		{
			
			t=clock();//renew time
			//particularly i=1
			if(i==1)
			{
				printandclear(1,x-1,21,color,"|||");
				for(int j=-1;j<=1;j++)
					site[x+j][21]=2;
			}
			//particularly i=23
			else if(i==22)
			{
				printandclear(0,x-1,1,color,"|||",3);
				for(int j=-1;j<=1;j++)
					site[x+j][1]=0;
			}
			//move front delete back
			//site[][]=2, there is a bullet
			else
			{
				printandclear(1,x-1,22-i,color,"|||");
				printandclear(0,x-1,23-i,color,"|||",3);
				for(int j=-1;j<=1;j++)
				{
					site[x+j][22-i]=2;
					site[x+j][23-i]=0;
				}
			}
			i++;//move front
			
		}
		break;
	}*/
}