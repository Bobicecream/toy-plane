#ifndef myfunction
#define myfunction

#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <string>
#include <vector>
#define U 72
#define R 77
#define D 80
#define L 75
#define RED 4
#define GREEN 2
#define BLUE 1
#define LIGHT 8
#define BACK 16 

using namespace std;

void gotoxy(int,int);
int direction();
void clear(int,int,int);
template <class T>
void color(int r,T read)
{
    //傳回一個標準輸出的handle
 HANDLE h = GetStdHandle ( STD_OUTPUT_HANDLE ); 
 WORD wOldColorAttrs;
 //建立console screen buffer的結構變數 
 CONSOLE_SCREEN_BUFFER_INFO csbiInfo;


 //傳回console視窗的資訊 ，儲存在之前建立的結構變數中 
 GetConsoleScreenBufferInfo(h, &csbiInfo);
 //保留console視窗的顏色屬性，以便在輸出後回覆 
 wOldColorAttrs = csbiInfo.wAttributes;
 //設定新的顏色 
 SetConsoleTextAttribute ( h, r );
      	
 
 cout <<read;
 //回覆原來的顏色屬性 
 SetConsoleTextAttribute ( h, wOldColorAttrs);
}
template <class T>
void printandclear(bool condition, int x,int y, T read, int l=1)
{
	if(condition)
	{
		gotoxy(x,y);
		cout <<read;
	}
	else
	{
		gotoxy(x,y);
		clear(x,y,l);
	}
}
template <class T>
void printandclear(bool condition, int x,int y,int pcolor, T read, int l=1)
{
	if(condition)
	{
		gotoxy(x,y);
		color(pcolor,read);
	}
	else
	{
		gotoxy(x,y);
		clear(x,y,l);
	}
}


#endif
