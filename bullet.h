#ifndef bullet_h
#define bullet_h

#include "map.h"

class bullet :public map
{
	int x;//x coordinate
	bool s;//決定是否印出飛彈
	int color;//the color of the bullet
	int t;//上次時間
	int i;//related to y coordinate
	void move(const int);
	int bulletlevel;
public:
	friend class plane;

	bullet(const int);
	void print();
};
#endif