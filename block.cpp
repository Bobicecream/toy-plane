
#include "block.h"


int vlev=0;

const int PLANE::starttime=time(0);
const int PLANE::gap[]={500,400,300,150};
const int PLANE::blockmoney[]={10,20,40,80};
const int PLANE::twoblockgap[]={800,700,600,500};
const int PLANE::blockattack[]={30,120,400,900};
const int PLANE::uptime[]={50,90,140,INT_MAX};

block::block(int a) 
{
	
	x=a%25;//0<=x<25
	//x=a%23+1;
	y=0;
	t=-2000;
	d=1;//exists
	
}

void acceleration(const int acctime)
{
	if(time(0)-PLANE::starttime>=acctime-5 && time(0)-PLANE::starttime<=acctime-3)
	{
		gotoxy(10,12);
		color(RED+LIGHT,"warning");
	}
	if(time(0)-PLANE::starttime==acctime-2)
		clear(10,12,7);
	if(time(0)-PLANE::starttime==acctime)
		++vlev;
	
}

bool block::print()
{
	if(d==0)
	{
		color(RED+LIGHT,"error2");
		Sleep(20000);
	}
	acceleration(PLANE::uptime[vlev]);
	
	if(d)//exists
	{
		
		
		//there is a bullet in front of *this
		//add points	
		if(site[x][y]==2)
		{
			gotoxy(x,y-1);
			cout <<" ";
			PLANE::point.points+=PLANE::blockmoney[vlev]*((22-y)/7+1);
			PLANE::point.p=1;
			site[x][y-1]=0;
			d=0;//not exist
			return 0;//be deleted
		}
		if(clock()-t>=PLANE::gap[vlev] && y<22)
		{
			t=clock();//renew time
			//particularly y=0
			if(y==0)
			{
				gotoxy(x,y);
				color(BACK*GREEN," ");
				site[x][y]=3;
			}
			//move front delete back
			else
			{
				gotoxy(x,y);
				color(BACK*GREEN," ");
				gotoxy(x,y-1);
				cout<<" ";
				site[x][y]=3;
				site[x][y-1]=0;
			}
			y++;//move front
		}
		//particularly y=22
		//
		if(y==22)
		{
			if(site[x][y-1]==2)
			{
				PLANE::point.points+=PLANE::blockmoney[vlev];
				PLANE::point.p=1;
				gotoxy(x,21);
				cout<<" ";
				site[x][y-1]=0;			
				d=0;//not exist
				return 0;//be deleted
			}
			if(clock()-t>=600)
			{
				PLANE::point.points-=PLANE::blockattack[vlev];
				PLANE::point.p=1;
				gotoxy(x,21);
				cout<<" ";
				site[x][y-1]=0;			
				d=0;//not exist
				return 0;//be deleted
			}
			
		}
		//maintainance
		if(d!=1)
		{
			d=1;
			color(RED+LIGHT,"errorerrorerror");
			Sleep(20000);
		}
		
	}
	return 1;
}

bool block::getd()
{
	return d;
}

int block::getvlev()
{
	return vlev;
}

block2::block2(int a) :block(a)
{
	blockcolor=BACK*(GREEN+LIGHT);
	hp=2;
}
bool block2::print()
{
	
	if(d==0)
	{
		color(RED+LIGHT,"error1");
		Sleep(20000);
	}
	acceleration(PLANE::uptime[vlev]);
	
	if(hp==1)
		return block::print();
	if(hp==2)
	{
		if(d)//exists
		{
			//there is a bullet in front of *this
			//add points
			if(site[x][y]==2)
			{
				hp--;
				blockcolor=BACK*GREEN;
				site[x][y]=0;
				gotoxy(x,y-1);
				color(blockcolor," ");
			}
			if(clock()-t>=PLANE::gap[vlev] && y<22)
			{
				t=clock();//renew time
				//particularly y=0
				if(y==0)
				{
					gotoxy(x,y);
					color(blockcolor," ");
					site[x][y]=4;
				}
				//move front delete back
				else
				{
					gotoxy(x,y);
					color(blockcolor," ");
					gotoxy(x,y-1);
					cout<<" ";
					site[x][y]=4;
					site[x][y-1]=0;
				}
				y++;//move front
			}
			//particularly y=22
			//
			if(y==22)
			{
				if(site[x][y-1]==2)
				{
					hp--;
					blockcolor=BACK*GREEN;
					site[x][y-1]=0;
					gotoxy(x,y-1);
					color(blockcolor," ");
				}
				if(clock()-t>=600)
				{
					PLANE::point.points-=PLANE::blockattack[vlev];
					PLANE::point.p=1;
					gotoxy(x,21);
					cout<<" ";
					site[x][y-1]=0;			
					d=0;//not exist
					return 0;//be deleted
				}
				
			}
			//maintainance
			if(d!=1)
			{
				d=1;
				color(RED+LIGHT,"errorerrorerror");
				Sleep(20000);
			}
			
		}
	}
	
	return 1;
}

