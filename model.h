#ifndef model_h
#define model_h

#include "map.h"
#include "myheader.h"
#include "bullet.h"

class model :public map
{
	
	
	int level;//level
	int experience;//experience
public:
	model(const int);
	virtual void move(const int)=0;
	virtual void shoot(bullet *)=0;
	int getlevel();
	void setexperience(int);
protected:
	int x;//x coordinate
	int y;//y coordinate
	void update();//update level
	const int bulletcolor;
	int t;

};

class plane :public model
{
	void print(const bool);
public:
	plane(const int);
	virtual void shoot(bullet *);
	virtual void move(const int);
	
	

};



#endif