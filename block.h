#ifndef block_h
#define block_h

#include "map.h"
#include "grade.h"



class block :public map
{
public:
	
	block(int);
	bool print();
	
	bool getd();
	int getvlev();
protected:
	int x;//x coordinate
	int y;//y coordinate
	int t;//上次時間
	bool d;//是否存在
	
};

class block2 :public block
{
public:

	block2(int);
	bool print();
protected:
	int hp;
	int blockcolor;
};

namespace PLANE
{
	extern const int gap[];
	extern const int blockmoney[];
	extern const int twoblockgap[];
	extern const int blockattack[];
	extern const int uptime[];
	extern const int starttime;
}


#endif