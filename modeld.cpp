#include "model.h"

const int uplevel[]={100,300,500,800,4000,7000,13000,22000,40000,100000,INT_MAX};


model::model(const int c) :bulletcolor(c)
{
	level=0;
	x=5;
	y=24;
	experience=0;
	t=0;
}
void model::update()
{
	level++;
	//print level
	gotoxy(36,8);
	cout <<level;
}
int model::getlevel()
{
	return level;
}
void model::setexperience(int set)
{
	experience=set;
	if(experience>=uplevel[level])
	{
		update();
		gotoxy(37,5);
		cout <<uplevel[level];
	}
}

plane::plane(const int c) :model(c)
{
	gotoxy(5,23);
	cout <<0;
	gotoxy(4,24);
	cout <<"000";
}
void plane::print(const bool c)
{
	printandclear(c,x,y-1,0);
	printandclear(c,x-1,y,"000",3);
	
}
void plane::move(const int m)
{
	
		
		print(0);
		switch(m)
		{
		/*case U:
			y--;
			break;
		case D:
			y++;
			break;*/
		case R:
			if(x<23)
				x++;
			break;
		case L:
			if(x>1)
				x--;
			break;

		}
		print(1);
	
}

void plane::shoot(bullet *b)
{
	if(clock()-t>=80)
	{
		t=clock();
		if(b->i==1)
		{
			b->s=1;
			b->x=x;
			if(getlevel()>=2 && getlevel()<=4)
				b->bulletlevel=2;
			if(getlevel()>=5 && getlevel()<=11)
				b->bulletlevel=3;
			PLANE::point.points-=getlevel()/2*12;
		}
	}
}